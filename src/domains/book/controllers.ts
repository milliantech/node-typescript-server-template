import express from 'express';

export default {
  index: (_req: express.Request, res: express.Response) => {
    res.send('book#index');
  },
  show: (_req: express.Request, res: express.Response) => {
    const page =  _req.query.page;
    const bookId = _req.params.bookdId;
    res.send(`book#show#${bookId}#${page}`);
  },
};
