import express from 'express';
import * as bodyParser from "body-parser";

import bookRoutes from './domains/book/routes';
import reviewRoutes from './domains/review/routes';

const server = express();
server.use(bodyParser.urlencoded({extended: false}));
server.use(bodyParser.json());

server.use('/books', bookRoutes);
server.use('/reviews', reviewRoutes);

server.use('/_healthcheck', (_req, res) => {
  res.status(200).json({ uptime: process.uptime() });
});

export default server;
